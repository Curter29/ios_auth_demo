//
//  AuthDemoCoordinator.swift
//  AuthDemo
//
//  Created by Aleksander Tooszovsky on 28/06/2019.
//  Copyright © 2019 none. All rights reserved.
//

import UIKit
import InstructionsModule

final class AuthDemoCoordinator {
    public var navigationController: UINavigationController?

    func start() -> UIViewController? {
        let first = InstructionsViewController.init(view: InstructionFirstView.self)
        first.onNext = { [weak self] in
            self?.showSecondStep()
        }
        navigationController = UINavigationController(rootViewController: first)
        return navigationController
    }

    func showSecondStep() {
        let second = InstructionsViewController.init(view: InstructionSecondView.self)
        second.onNext = { [weak self] in
            self?.showCamera()
        }
        navigationController?.pushViewController(second, animated: true)
    }

    func showCamera() {

    }

    func showStatus() {

    }

    func showResult() {

    }
}
