//
//  AppDelegate.swift
//  AuthDemo
//
//  Created by Aleksander Tooszovsky on 28/06/2019.
//  Copyright © 2019 none. All rights reserved.
//

import UIKit
import InstructionsModule

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var coordinator: AuthDemoCoordinator?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        coordinator = AuthDemoCoordinator()
        window?.rootViewController = coordinator?.start()
        window?.makeKeyAndVisible()
        return true
    }

}

