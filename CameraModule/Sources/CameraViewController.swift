//
//  CameraViewController.swift
//  CameraModule
//
//  Created by Aleksander Tooszovsky on 29/06/2019.
//  Copyright © 2019 none. All rights reserved.
//

import UIKit

class TempViewController: UIViewController, FrameExtractorDelegate {

    var frameExtractor: FrameExtractor!
    var images = [UIImage]()
    var imagesToSend = [UIImage]()
    var imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 360, height: 480))

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.addSubview(imageView)
        frameExtractor = FrameExtractor()
        frameExtractor.delegate = self

    }

    func captured(image: UIImage) {
        images.append(image)
        imageView.image = image
        if images.count == 24 {
            imagesToSend = [images[0], images[3], images[11]]
                .filter { detect(image: $0) }
            images.removeAll()
        }
        if imagesToSend.count == 3 {
            print("send")
        }
    }

    func detect(image: UIImage) -> Bool {
        let imageOptions =  NSDictionary(object: NSNumber(value: 5) as NSNumber, forKey: CIDetectorImageOrientation as NSString)
        let personciImage = CIImage(cgImage: image.cgImage!)
        let accuracy = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
        let faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: accuracy)
        let faces = faceDetector?.features(in: personciImage, options: imageOptions as? [String: AnyObject])

        if let face = faces?.first as? CIFaceFeature,
            face.hasLeftEyePosition && face.hasRightEyePosition {
            return true
        }
        return false
    }
}
