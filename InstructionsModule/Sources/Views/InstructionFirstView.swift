//
//  InstructionFirstView.swift
//  InstructionsModule
//
//  Created by Aleksander Tooszovsky on 28/06/2019.
//  Copyright © 2019 none. All rights reserved.
//

import UIKit

public class InstructionFirstView: UIView, NextStepable {
    public override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        configureInitialState()
        addComponents()
        configureConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    var title: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 17)
        label.textColor = .black
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    var subtitle: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = #colorLiteral(red: 0.6078431373, green: 0.6078431373, blue: 0.6078431373, alpha: 1)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    var image: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()

    var images = [UIImageView]()

    var button: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
        button.layer.cornerRadius = 10
        return button
    }()

    var stackView: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()

    func addComponents() {
        images.append(UIImageView(image: #imageLiteral(resourceName: "ManInGreenCycle")))
        images.append(UIImageView(image: #imageLiteral(resourceName: "ManInEvning")))
        images.append(UIImageView(image: #imageLiteral(resourceName: "ManInNight")))
        addSubview(stackView)
        stackView.addSubview(title)
        stackView.addSubview(subtitle)
        images.forEach {
            stackView.addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        stackView.addSubview(button)
    }

    func configureInitialState() {
        title.text = "Find right light position"
        subtitle.text = "Face must be equally illuminated"
        button.setTitle("CONTINUE", for: .normal)
    }

    func configureConstraints() {
        var constraints: [NSLayoutConstraint] = []
        let views = ["title": title,
                     "stackView": stackView,
                     "subtitle": subtitle,
                     "image1": images[0],
                     "image2": images[1],
                     "image3": images[2],
                     "button": button]
        stackView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        stackView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        let stackVertical = NSLayoutConstraint.constraints(withVisualFormat: "V:|-(>=16)-[stackView]-(>=16)-|",
                                                           options: [],
                                                           metrics: nil,
                                                           views: views)
        let stackHorisontal = NSLayoutConstraint.constraints(withVisualFormat: "H:|-(>=8)-[stackView]-(>=8)-|",
                                                             options: [],
                                                             metrics: nil,
                                                             views: views)
        constraints += stackVertical
        constraints += stackHorisontal
        images[0].centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        images[1].centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        images[2].centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true

        let verticalLayout = NSLayoutConstraint.constraints(withVisualFormat:
            "V:|[title(24)]-16-[subtitle(24)]-16-[image1(114)]-24-[image2(114)]-24-[image3(114)]-64-[button(48)]|",
                                                            options: [],
                                                            metrics: nil,
                                                            views: views)
        let horisontalTitle = NSLayoutConstraint.constraints(withVisualFormat: "H:|-16-[title]-16-|",
                                                             options: [],
                                                             metrics: nil,
                                                             views: views)
        let horisontalSubtitle = NSLayoutConstraint.constraints(withVisualFormat: "H:|-16-[subtitle]-16-|",
                                                                options: [],
                                                                metrics: nil,
                                                                views: views)
        let horisontalButton = NSLayoutConstraint.constraints(withVisualFormat: "H:|-48-[button(300)]-48-|",
                                                                options: [],
                                                                metrics: nil,
                                                                views: views)
        constraints += horisontalTitle
        constraints += horisontalSubtitle
        constraints += horisontalButton

        constraints += verticalLayout
        NSLayoutConstraint.activate(constraints)
    }
}
