//
//  NextSteppable.swift
//  InstructionsModule
//
//  Created by Aleksander Tooszovsky on 29/06/2019.
//  Copyright © 2019 none. All rights reserved.
//

import Foundation

protocol NextStepable {
    var button: UIButton { get set }
}
