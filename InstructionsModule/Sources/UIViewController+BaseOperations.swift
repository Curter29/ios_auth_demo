//
//  UIViewController+BaseOperations.swift
//  InstructionsModule
//
//  Created by Aleksander Tooszovsky on 28/06/2019.
//  Copyright © 2019 none. All rights reserved.
//

import UIKit

extension UIViewController {
    public func makeTransparentNavigationBar(_ transparent: Bool = true) {
        navigationController?.navigationBar.setBackgroundImage(transparent ? UIImage() : nil,
                                                               for: .any,
                                                               barMetrics: .default)
        navigationController?.navigationBar.isTranslucent = transparent
    }

    /// just removes the line
    public func removeNavigationBarLine() {
        for subview in navigationController?.view.subviews ?? [] {
            hideShadowSubviews(subview)
        }
    }

    private func hideShadowSubviews(_ view: UIView) {
        for subview in view.subviews {
            hideShadowSubviews(subview)
        }
        if view is UIImageView && view.frame.height <= 1 {
            view.isHidden = true
        }
    }

    public func removeBackButtonTitle() {
        let backItem = UIBarButtonItem()
        backItem.title = " "
        navigationItem.backBarButtonItem = backItem
    }
}
