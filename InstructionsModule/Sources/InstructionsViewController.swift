//
//  InstructionsViewController.swift
//  InstructionsModule
//
//  Created by Aleksander Tooszovsky on 28/06/2019.
//  Copyright © 2019 none. All rights reserved.
//

import UIKit

public class InstructionsViewController: UIViewController {

    public var onNext: (() -> Void)?
    let customView: UIView.Type

    public init(view: UIView.Type) {
        customView = view
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public override func loadView() {
        super.loadView()
        view = customView.init(frame: .zero)
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        guard let view = view as? NextStepable else { return }
        view.button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        makeTransparentNavigationBar()
        removeNavigationBarLine()
    }

    @objc
    func buttonAction(sender: Any) {
        guard let onNext = onNext else { return }
        onNext()
    }

}
